import json
import threading
import Adafruit_DHT
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt

print('\n')
print('----------------------------------------------------')
print('|                   MQTT SAMPLE 1                  |')
print('----------------------------------------------------')
print('\n')

# Set sensor type: Options are DHT11, DHT22 or AM2302
GPIO.setmode(GPIO.BCM)
sensor = Adafruit_DHT.DHT11
gpio = 17  # Set GPIO sensor is connected to
relay = 18  # Set GPIO Relay is connected to
GPIO.setup(relay, GPIO.OUT)  # Setup Mode of GPIO

DEVICE_ID = 'f25b5c40-31a3-46e7-a900-7b2ba1c20f92'  # must change to your deviceId
CLIENT_ID = '23SJ6MGM5TONQK4EJLY06P6'  # must change to your clientId
MQTT_SERVER = '172.16.110.79'  # Podiot Mqtt Broker Address (mqtt.thingscloud.ir)
MQTT_PORT = int('8883')  # Podiot Mqtt Broker Port (1883)

# Change this appropiately
SENSOR_NAME_1 = 'temperature'
SENSOR_NAME_2 = 'humidity'

# publish topics
publishTopicReported = 'dvcasy/twin/update/reported'
publishTopicGet = 'dvcasy/twin/get'
publishTopicDelete = 'dvcasy/twin/delete'

# subscribe topics
subscribeTopicOrigin = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/#'
subscribeTopicDesired = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/update/desired'
subscribeTopicDocument = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/update/document'
subscribeTopicAccepted = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/response/accepted'
subscribeTopicRejected = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/response/rejected'


class Payload(object):
    def __init__(self, msg):
        self.__dict__ = json.loads(msg)


# The callback for when the client receives a CONNECT response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected to Podiot with result code " + str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(subscribeTopicOrigin)
    # if connection successful start publishing data
    if rc == 0:
        publishTempData()


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    json_msg = Payload(msg.payload)
    content = json_msg.content
    main_msg = json.loads(content)
    json_object = json.loads(str(msg.payload.decode()))
    json_object['content'] = json.loads(str(json_object['content']))
    json_formatted_str = json.dumps(json_object, indent=2)
    if msg.topic == subscribeTopicDesired:
        print("\nMessage Received from DesiredTopic: " + json_formatted_str)
        command = main_msg['deviceTwinDocument']['attributes']['desired']['airCondition']
        print(f'command is : {command}')
        if command == 'on':
            GPIO.output(relay, GPIO.HIGH)
        if command == 'off':
            GPIO.output(relay, GPIO.LOW)
    if msg.topic == subscribeTopicDocument:
        print("\nMessage Received from DocumentTopic: " + json_formatted_str)
    if msg.topic == subscribeTopicAccepted:
        print("\nMessage Received from AcceptedTopic: " + json_formatted_str)
    if msg.topic == subscribeTopicRejected:
        print("\nMessage Received from RejectedTopic: " + json_formatted_str)


# Publish random temp data between 10-100
def publishData():
    # Use read_retry method. This will retry up to 15 times to
    # get a sensor reading (waiting 2 seconds between each retry).
    humidity, temperature = Adafruit_DHT.read_retry(sensor, gpio)
    message = json.dumps({"deviceTwinDocument": {"attributes": {"reported": {SENSOR_NAME_1: str(temperature), SENSOR_NAME_2: str(humidity)}}}}, indent=2)
    print("Publish Data: ", message)
    client.publish(publishTopicReported, message)


def publishTempData():
    publishData()
    threading.Timer(60, publishTempData).start()


client = mqtt.Client(CLIENT_ID)
client.on_connect = on_connect
client.on_message = on_message
client.connect(MQTT_SERVER, MQTT_PORT, 60, bind_address="")
client.loop_forever()
